<?php
spl_autoload_register(function ($classname){
    $classname = str_replace('\\', '/', $classname);

    if (!include_once("$classname.php")) {
        throw new Exception("class $classname not found");
    }
});
