<?php

/**
 * Class MachineClass
 */
class MachineClass
{
    protected $response;
    protected $postData;

    public function __construct()
    {

    }

    /**
     * @param array $postData
     * @return $this
     */
    public function recieve(array $postData)
    {
        $this->postData = $postData;
        $this->validate();
        return $this;
    }

    /**
     * @return mixed
     */
    public function response()
    {
        return $this->response;
    }

    /**
     *
     */
    protected function validate()
    {
        $rules = $this->validationRules();

        foreach ($rules as $fieldName => $ruleString) {
            $fieldRules = explode('|', $ruleString);

            foreach ($fieldRules as $rule) {
                $params = explode(':', $rule);
                $ruleName = $params[0];

                try {
                    if (isset($params[1])) {
                        $check = $this->$ruleName($fieldName, $params[1]);
                    } else {
                        $check = $this->$ruleName($fieldName);
                    }

                    if (! $check) {
                        throw new Exception("Parameter $fieldName not pass rule $ruleName");
                    }
                } catch(Exception $e) {
                    echo nl2br($e->getMessage() . PHP_EOL);
                }
            }
        };
    }

    /**
     * Правила валидации входящих значений.
     * Pipe как разделитель между правилами, двоеточие - разделитель между именем правила и значением ограничения
     *
     * @return array
     */
    protected function validationRules():array
    {
        return [
            'serial' => 'required|isNumeric|length:15',
            'time' => 'required|isDateTime',
            'connect_freq' => 'isNumeric|minLength:1|maxLength:2',
            'firmware' => 'isNumeric|maxLength:32',
        ];
    }

    /**
     * Проверяет наличие ключа в полученном массиве данных
     *
     * @param string $fieldName имя ключа
     * @return bool
     */
    protected function required(string $fieldName):bool
    {
        return array_key_exists($fieldName, $this->postData);
    }

    /**
     * Проверяет содержит ли значение элемента полученного массива данных числовое значение
     *
     * @param string $fieldName имя ключа проверяемого элемента
     * @return bool
     */
    protected function isNumeric(string $fieldName):bool
    {
        return is_numeric($this->postData[$fieldName]);
    }

    /**
     * Проверяет длину значения элемента полученного массива данных
     *
     * @param string $fieldName имя ключа проверяемого элемента
     * @param string $length длина проверяемого элемента
     * @return bool
     */
    protected function length(string $fieldName, string $length):bool
    {
        return mb_strlen($this->postData[$fieldName]) == (int) $length;
    }

    /**
     * Проверяет минимальную длину значения элемента полученного массива данных
     *
     * @param string $fieldName имя ключа проверяемого элемента
     * @param string $length минимальная длина проверяемого элемента
     * @return bool
     */
    protected function minLength(string $fieldName, string $length):bool
    {
        return mb_strlen($this->postData[$fieldName]) >= (int) $length;
    }

    /**
     * Проверяет максимальную длину значения элемента полученного массива данных
     *
     * @param string $fieldName имя ключа проверяемого элемента
     * @param string $length максимальная длина проверяемого элемента
     * @return bool
     */
    protected function maxLength(string $fieldName, string $length):bool
    {
        return mb_strlen($this->postData[$fieldName]) <= (int) $length;
    }

    /**
     * Проверяет соответствие значения элемента полученного массива данных формату DateTime
     *
     * @param string $fieldName имя ключа проверяемого элемента
     * @return bool
     */
    protected function isDateTime(string $fieldName):bool
    {
        $pattern = '/(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/';
        return preg_match($pattern, $this->postData[$fieldName]);
    }
}