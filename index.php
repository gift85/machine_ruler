<?php

require_once('autoload.php');

echo 'hello machine' . PHP_EOL;
?>
<form method="post">
    <div>
        <label for="serial">serial:</label>
        <input type="text" id="serial" name="serial" value="<?= $_POST['serial'] ?? ''?>">
    </div>
    <div>
        <label for="time">time:</label>
        <input type="text" id="time" name="time" value="<?= $_POST['time'] ?? ''?>">
    </div>
    <div>
        <label for="connect_freq">connect_freq:</label>
        <input type="text" id="connect_freq" name="connect_freq" value="<?= $_POST['connect_freq'] ?? ''?>">
    </div>
    <div>
        <label for="firmware">firmware:</label>
        <input type="text" id="firmware" name="firmware" value="<?= $_POST['firmware'] ?? ''?>">
    </div>
    <button>Send</button>
</form>
<?php
$data = $_POST;
//echo '<pre>';
//var_export($_POST);
//echo '</pre>';
/*$data = [
    'firmware' => '100',
    'connect_freq' => '3',
    'time' => '222',
    'serial' => '111'
];*/

$response = (new MachineClass())
    ->recieve($data)
    ->response();